<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.docbook.org/xml/4.5/docbookx.dtd">
<refentry>
  <refentryinfo>
    <title>ExactImage Manual</title>
    <date>04/11/2010</date>
    <productname>econvert</productname>
    <authorgroup>
      <author>
        <firstname>Jakub</firstname>
        <surname>Wilk</surname>
        <contrib>Wrote this manual page for the Debian system.</contrib>
        <address>
          <email>jwilk@debian.org</email>
        </address>
      </author>
      <author>
        <othername><ulink url='https://exactcode.com/opensource/exactimage/'/></othername>
        <contrib>This manual page incorporates texts found on the ExactImage homepage.</contrib>
        <!-- See <20100411100610.GA4243@jwilk.net> in the debian-email archives. -->
      </author>
    </authorgroup>
    <legalnotice>
      <para>This manual page was written for the Debian system (and may be used
        by others).</para>
      <para>Permission is granted to copy, distribute and/or modify this
        document under the terms of the GNU General Public License,
        Version 2 or (at your option) any later version published by
        the Free Software Foundation.</para>
      <para>On Debian systems, the complete text of the GNU General Public
        License can be found in
        <filename>/usr/share/common-licenses/GPL-2</filename>.
      </para>
    </legalnotice>
  </refentryinfo>
  <refmeta>
    <refentrytitle>econvert</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>
  <refnamediv>
    <refname>econvert</refname>
    <refpurpose>image converter of the ExactImage toolkit</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>econvert</command>
      <arg choice='opt' rep='repeat'><replaceable>option</replaceable></arg>
      <arg choice='opt' rep='repeat'>
        <group choice='req'>
          <arg choice='plain'><option>-i</option></arg>
          <arg choice='plain'><option>--input</option></arg>
        </group>
        <arg choice='plain'><replaceable>input-file</replaceable></arg>
      </arg>
      <arg choice='opt' rep='repeat'>
        <group choice='req'>
          <arg choice='plain'><option>-o</option></arg>
          <arg choice='plain'><option>--output</option></arg>
        </group>
        <arg choice='plain'><replaceable>output-file</replaceable></arg>
      </arg>
    </cmdsynopsis>
    <cmdsynopsis>
      <command>econvert</command>
      <group choice='req'>
        <arg choice='plain'><option>-h</option></arg>
        <arg choice='plain'><option>--help</option></arg>
      </group>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Description</title>
    <para>ExactImage is a fast C++ image processing library. Unlike many other library
frameworks it allows operation in several color spaces and bit depths
natively, resulting in low memory and computational requirements.</para>
    <para><command>econvert</command> is a command line frontend for the image processing library and
mimics ImageMagick's convert. However econvert syntax is not compatible
with convert.</para>
  </refsection>
  <refsection>
    <title>Options</title>
    <refsection>
      <title>Input/output</title>
      <variablelist>
        <varlistentry>
          <term><option>-i <replaceable>file</replaceable></option></term>
          <term><option>--input <replaceable>file</replaceable></option></term>
          <listitem>
            <para>Read image from the specified file. Optionally, filenames can be prefixed
with format name, e.g. <literal>jpg:-</literal> or <literal>raw:rgb8-dump</literal>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>-o <replaceable>file</replaceable></option></term>
          <term><option>--output <replaceable>file</replaceable></option></term>
          <listitem>
            <para>Save output image to the specified file. Optionally, filenames can be
prefixed with format name, e.g. <literal>jpg:-</literal> or <literal>raw:rgb8-dump</literal>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>-a <replaceable>file</replaceable></option></term>
          <term><option>--append <replaceable>file</replaceable></option></term>
          <listitem>
            <para>Append output image to the specified file. Existing image must have the
same width as the appended one. Optionally, filenames can be prefixed
with format name, e.g. <literal>jpg:-</literal> or <literal>raw:rgb8-dump</literal>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--compress <replaceable>method</replaceable></option></term>
          <listitem>
            <para>Use the specified compression method for writing images, e.g. <literal>G3</literal>,
<literal>G4</literal>, <literal>Zip</literal>, … The default depends on the output format.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--decompress <replaceable>method</replaceable></option></term>
          <listitem>
            <para>Use the specified decompression method for reading images.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--quality <replaceable>n</replaceable></option></term>
          <listitem>
            <para>Use the specified quality used for writing compressed images. Valid
values are integers from 0 to 100. The default is 75.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--resolution <replaceable>xdpi</replaceable> <replaceable>[ydpi]</replaceable></option></term>
          <listitem>
            <para>Set metadata resolution.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--size <replaceable>width</replaceable> <replaceable>height</replaceable></option></term>
          <listitem>
            <para>Set image dimensions for raw images.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--split <replaceable>file…</replaceable></option></term>
          <listitem>
            <para>Split the image in Y-direction into multiple images.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsection>
    <refsection>
      <title>Scaling, cropping, transformations</title>
      <variablelist>
        <varlistentry>
          <term><option>--bicubic-scale <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Scale image data with bi-cubic filter. Use scale factor <replaceable>x</replaceable>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--bilinear-scale <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Scale image data with bi-linear filter. Use scale factor <replaceable>x</replaceable>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--box-scale <replaceable>x</replaceable></option></term>
          <listitem>
            <para>(Down)scale image data with box filter. Use scale factor <replaceable>x</replaceable>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--ddt-scale <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Scale image data with data dependent triangulation. Use scale factor <replaceable>x</replaceable>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--nearest-scale <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Scale image data to nearest neighbour. Use scale factor <replaceable>x</replaceable>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--scale <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Scale image data using a method suitable for specified factor <replaceable>x</replaceable>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--thumbnail <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Quick and dirty down-scale for a thumbnail. Use scale factor <replaceable>x</replaceable>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--crop <replaceable>x</replaceable> <replaceable>y</replaceable> <replaceable>w</replaceable> <replaceable>h</replaceable></option></term>
          <listitem>
            <para>Crop the specified area out of the image.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--fast-auto-crop</option></term>
          <listitem>
            <para>Fast auto crop.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--flip</option></term>
          <listitem>
            <para>Flip the image vertically.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--flop</option></term>
          <listitem>
            <para>Flip the image horizontally.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--rotate <replaceable>n</replaceable></option></term>
          <listitem>
            <para>Rotate the image <replaceable>n</replaceable> degrees.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsection>
    <refsection>
      <title>Colors</title>
      <variablelist>
        <varlistentry>
          <term><option>--brightness <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Change the image brightness.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--contrast <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Change the image contrast.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--saturation <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Change the image saturation.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--lightness <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Change the image lightness.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--hue <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Change the image hue.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--negate</option></term>
          <listitem>
            <para>Negate the image.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--gamma <replaceable>x</replaceable></option></term>
          <listitem>
            <para>Change the image gamma.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--normalize</option></term>
          <listitem>
            <para>Transform the image to span the full color range.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--colorspace colorspace</option></term>
          <listitem>
            <para>Convert image colorspace. Valid values are: <literal>BW</literal>, <literal>BILEVEL</literal>,
<literal>GRAY</literal>, <literal>GRAY1</literal>, <literal>GRAY2</literal>, <literal>GRAY4</literal>, <literal>RGB</literal>, <literal>YUV</literal> and <literal>CYMK</literal>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--floyd-steinberg <replaceable>n</replaceable></option></term>
          <listitem>
            <para>Perform Floyd-Steinberg dithering using <replaceable>n</replaceable> shades.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--riemersma <replaceable>n</replaceable></option></term>
          <listitem>
            <para>Perform Riemersma dithering using <replaceable>n</replaceable> shades.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsection>
    <refsection>
      <title>Filters</title>
      <variablelist>
        <varlistentry>
          <term><option>--blur stdev</option></term>
          <listitem>
            <para>Perform gaussian blur with standard deviation <replaceable>stdev</replaceable>.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--deinterlace</option></term>
          <listitem>
            <para>Shuffle every 2nd line.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--edge</option></term>
          <listitem>
            <para>Detect edges.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--convolve <replaceable>x11</replaceable> <replaceable>x12</replaceable> <replaceable>…</replaceable> <replaceable>x1n</replaceable> <replaceable>…</replaceable> <replaceable>xn1</replaceable> <replaceable>xn2</replaceable> <replaceable>…</replaceable> <replaceable>xnn</replaceable></option></term>
          <listitem>
            <para>Convolve the image using the specified convolution matrix.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsection>
    <refsection>
      <title>Drawing</title>
      <variablelist>
        <varlistentry>
          <term><option>--foreground <replaceable>color</replaceable></option></term>
          <listitem>
            <para>Set foreground color.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--background <replaceable>color</replaceable></option></term>
          <listitem>
            <para>Set background color.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--font <replaceable>font</replaceable></option></term>
          <listitem>
            <para>Use the specified font for drawing text.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--text-rotation <replaceable>n</replaceable></option></term>
          <listitem>
            <para>Use the specified text rotation.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--text <replaceable>x1</replaceable> <replaceable>y1</replaceable> <replaceable>height</replaceable> <replaceable>text</replaceable></option></term>
          <listitem>
            <para>Draw text.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--stroke-width <replaceable>n</replaceable></option></term>
          <listitem>
            <para>Set stroke width for vector primitives.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><option>--line <replaceable>x1</replaceable> <replaceable>y1</replaceable> <replaceable>x2</replaceable> <replaceable>y2</replaceable></option></term>
          <listitem>
            <para>Draw a line.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsection>
    <refsection>
      <title>Help</title>
      <variablelist>
        <varlistentry>
          <term><option>-h</option></term>
          <term><option>--help</option></term>
          <listitem>
            <para>Display help text and exit.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsection>
  </refsection>
  <refsection>
    <title>Examples</title>
    <refsection>
      <title>Basics</title>
      <para>Image data must be read using <option>-i</option> or <option>--input</option>, processing routines are
selected by their name with two leading dashes (e.g. <option>--rotate</option>) and at any
point the data might be written into a file with <option>-o</option> or <option>--output</option>, for
example:</para>
      <screen>econvert -i lenea.tiff --box-scale 0.5 -o medium.png --box-scale 0.5 -o little.png</screen>
    </refsection>
    <refsection>
      <title>Lossless transformations of JPEG files</title>
      <para>The library delays image decoding as much as possible and provides lossless
algorithms to work on compressed data (such as JPEGs) directly:</para>
      <screen>econvert -i AV220-Scan.JPG --resolution 300x300 -o 1.jpg --rotate 90 -o 2.jpg --rotate 180 -o 3.jpg --rotate -90 -o 4.jpg --flip -o 5.jpg --flop -o 6.jpg --scale 0.25 -o thumb.jpg</screen>
      <para>In this example <filename>1.jpg</filename> will be created from the original JPEG DCT
coefficients, those coefficients will be rearranged and <filename>2.jpg</filename>,
<filename>3.jpg</filename>, <filename>4.jpg</filename>, <filename>5.jpg</filename> and <filename>6.jpg</filename> will be written without any
additional loss in quality. Only at the end, for image <filename>thumb.jpg</filename>, the
DCT will actually be decoded — but due to the accelerated JPEG scaling only
partially.</para>
    </refsection>
    <refsection>
      <title>Thumbnails of bi-level images</title>
      <para>When 1 bit, black and white, images are scaled down, the output often looks
bad, as the library algorithms always operate in the color-space the image
data is stored in. To work around this problem, the colorspace must be
changed (e.g. to 8 bit gray) before applying the box scaler. At the end the
result might be converted back to just a few shades of gray such as 2 or 4
bit:</para>
      <screen>econvert -i avision-bw-scan.pbm --colorspace gray8 --box-scale 0.125 --colorspace gray2 -o thumb.png</screen>
    </refsection>
    <refsection>
      <title>Faster JPEG down-scaling</title>
      <para>If you don't care about quality, only about throughput, you can force
nearest neighbor scaling by just specifying a scale factor the JPEG decoder
can accelerate (½, ¼, or ⅛) and apply the remaining scaling manually. To
achieve faster scaling with the effective factor ⅓:</para>
      <screen>econvert -i big.jpg --scale .5 --nearest-scale 0.66 -o thumb.jpg</screen>
    </refsection>
    <refsection>
      <title>Working with digital camera RAW data</title>
      <para>Wide range of digital camera RAW formats is supported. Usually decoding of
RAW data should be transparent and automatic, however some formats also are
also valid TIFF files and the embedded thumbnail might be picked by in
favour of the actual RAW content. This this cases the <application>dcraw</application> decoder can
explicitly be requested with the decoder prefix of the input parameter:</para>
      <screen>econvert -i dcraw:img_0123.cr2 …</screen>
      <para>To quickly extract the embedded thumbnail preview, specify <literal>thumb</literal> as
decompression method before the image is loaded:</para>
      <screen>econvert --decompress thumb -i dcraw:img_0123.cr2 …</screen>
    </refsection>
    <refsection>
      <title>Loading arbitrary raw data</title>
      <para>It is possible to load arbitrary raw data via the <literal>raw:</literal> codec
specification; color-depth (colorspace) and size for the raw data have to be
explicitly specified:</para>
      <screen>econvert --size 1696x32 --colorspace rgb8 -i raw:data-file …</screen>
    </refsection>
  </refsection>
  <refsection>
    <title>See also</title>
    <para>
      <citerefentry><refentrytitle>exactimage</refentrytitle><manvolnum>7</manvolnum></citerefentry>
    </para>
  </refsection>
</refentry>

<!-- vim:set ts=2 sw=2 et:-->

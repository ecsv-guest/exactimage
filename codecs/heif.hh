/*
 * Copyright (C) 2023 - 2024 René Rebe, ExactCODE GmbH
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2. A copy of the GNU General
 * Public License can be found in the file LICENSE.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANT-
 * ABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * Alternatively, commercial licensing options are available from the
 * copyright holder ExactCODE GmbH Germany.
 */

#include "Codecs.hh"

class HEIFCodec : public ImageCodec {
public:
  
  HEIFCodec ();;
  ~HEIFCodec ();
  
  virtual std::string getID () { return "HEIF"; };
  
  virtual int readImage (std::istream* stream, Image& im, const std::string& decompress);
  virtual bool writeImage (std::ostream* stream, Image& im,
			   int quality, const std::string& compress);
protected:
  static bool initialized;
};
